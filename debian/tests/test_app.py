import pytest

from flask import Flask, jsonify, request
from flask_jwt_simple import (
    JWTManager, jwt_required, create_jwt, get_jwt_identity
)

app = Flask(__name__)

app.config['JWT_SECRET_KEY'] = 'super-secret'
jwt = JWTManager(app)


@app.route('/unprotected', methods=['GET'])
def unprotected():
    return 'ok', 200


@app.route('/protected', methods=['GET'])
@jwt_required
def protected():
    return jsonify({'msg': 'ok'}), 200

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_unprotected(client):
    resp = client.get('/unprotected')
    assert b'ok' in resp.data
    assert 200 == resp.status_code


def test_protected_without_authorization(client):
    resp = client.get('/protected')
    assert b'{"msg":"Missing Authorization Header"}' in resp.data
    assert 401 == resp.status_code

